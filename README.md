clone source from  git
### Setup steps
clone source from  git
copy .env.example to .env
provide db connection in .env
composer install
php artisan migrate
php artisan jwt:secret
php artisan key:generate

### Seeder
php artisan db:seed UserSeeder

### To generate the swagger documentation file in url/api/documentation: 
php artisan l5-swagger:generate

