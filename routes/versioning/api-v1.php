<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(
    [
        'prefix' => 'v1',
        'name' => 'api',
        'namespace' => '\App\Modules\Api\Controllers',
    ],
    function ($router) {
        $router->group(['prefix' => 'auth', 'name' => 'auth.'], function ($router) {
            $router->post('/login', 'AuthController@login')->name('login.');
            $router->group(['middleware' => 'auth:api'], function ($router) {
                $router->post('/user/logout', 'AuthController@logout')->name('logout.');
            });
        });
        
        $router->group(['prefix' => 'store', 'name' => 'store.'], function ($router) {
            $router->get('/details/{id}', 'StoreController@showDetails')->name('get-store-detail');
            $router->get('/list', 'StoreController@getStores')->name('get-store-list');
            
            $router->group(['middleware' => 'auth:api'], function ($router) {
                $router->post('/create', 'StoreController@createStore')->name('create.');
                $router->put('/update/{id}', 'StoreController@updateStore')->name('update.');
                $router->delete('/delete/{id}', 'StoreController@delete')->name('delete.');
            });
        });

        $router->group(['prefix' => 'product', 'name' => 'product.'], function ($router) {
            $router->get('/details/{id}', 'ProductController@showDetails')->name('get-product-detail');
            $router->get('/list', 'ProductController@getProducts')->name('get-product-list');
            
            $router->group(['middleware' => 'auth:api'], function ($router) {
                $router->post('/create', 'ProductController@createProduct')->name('create.');
                $router->put('/update/{id}', 'ProductController@updateProduct')->name('update.');
                $router->delete('/delete/{id}', 'ProductController@delete')->name('delete.');
            });
        });
    }
);