<?php

return [
    'delete_succeeded'      => 'Delete succeeded!',
    'delete_failed'         => 'Delete failed!',
    'create_succeeded'      => 'Create succeeded!',
    'could_not_create'      => 'Could not create',
    'update_succeeded'      => 'Update succeeded!',
    'could_not_update'      => 'Could not update',
    'save_succeeded'        => 'Save succeeded!',
    'not_found'             => 'Not found',
    'get_data_success'      => 'get data success',
    'access_denied'         => 'Access denied',
];