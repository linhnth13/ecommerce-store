<?php

return [
    'delete_succeeded'      => 'Xóa thành công!',
    'delete_failed'         => 'Xóa không thành công!',
    'create_succeeded'      => 'Tạo thành công!',
    'could_not_create'      => 'Không thể tạo',
    'update_succeeded'      => 'Cập nhật thành công!',
    'could_not_update'      => 'Không thể cập nhật',
    'save_succeeded'        => 'Lưu thành công!',
    'not_found'             => 'Không tìm thấy',
    'get_data_success'      => 'Lấy dữ liệu thành công',
    'access_denied'         => 'Không có quyền truy cập',
];