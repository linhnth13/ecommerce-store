<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Thông tin xác thực này không khớp với hồ sơ của chúng tôi.',
    'password' => 'Mật khẩu đã cung cấp không đúng.',
    'throttle' => 'Quá nhiều lần đăng nhập. Vui lòng thử lại sau :seconds seconds.',
    'unauthenticated' => 'Thông tin đăng nhập không đúng',
    'login_successful' => 'Đăng nhập thành công',
    'incorrect_username_password' => 'Tên đăng nhập hoặc mật khẩu không đúng',
    'logout_successful' => 'Đăng xuất thành công',
];
