<?php

namespace App\Services;

use App\Models\Store;
use App\Models\User;
use App\CommomHelper;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class StoreService
{
    protected $defaultPath;
    
    public function __construct() {
        $this->defaultPath = "/media/stores/";
    }

    /**
     * Create a new store
     * @param Request $request
     * @param User $user
     * 
     * @return array $result
     */
    public function create($request, $user) {
        $user = auth('api')->user();
        if (!$user) {
            return [
                'message' => trans('api.access_denied'),
                'success' => false,
                'statusCode' => Response::HTTP_FORBIDDEN,
            ];
        }
        DB::beginTransaction();
        try{
            $data = $request->all();
            $dataInsert = [
                'user_id' => $user->id,
                'name' => $data['name'],
                'description' => $data['description'],
                'status' => (int)$data['status'],
            ];
            $newStore = Store::create($dataInsert);

            DB::commit();
            return [
                'message' => trans('api.create_succeeded'),
                'success' => true,
                'data' => $newStore
            ];
        } catch(\Exception $ex) {
            DB::rollBack();
            \Log::channel('daily')->error($ex->getMessage(), (array)$ex);
            return [
                'message' => trans('api.could_not_create'),
                'success' => false,
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR,
            ];
        }
    }

    /**
     * Update store
     * @param Request $request
     * @param User $user
     * 
     * @return array $result
     */
    public function update($id, $request, $user) {
        $store = Store::where('id', $id)->first();
        if (!$store) {
            $result['message'] = trans('api.not_found');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $user = auth('api')->user();
        if (!$user || ($user && $user->id != $store->user_id)) {
            return [
                'message' => trans('api.access_denied'),
                'success' => false,
                'statusCode' => Response::HTTP_FORBIDDEN,
            ];
        }
        $result = [
            'message' => trans('api.update_succeeded'),
            'success' => true,
            'statusCode' => Response::HTTP_OK,
            'data' => null,
        ];
        
        
        DB::beginTransaction();
        try{
            $data = $request->all();
            foreach ($data as $field => $value) {
                if (!empty($value)) {
                    switch ($field) {
                        case 'name':
                            $store->name = $value;
                            break;
                        case 'description':
                            $store->description = $value;
                            break;
                        case 'status':
                                $store->status = (int)$value;
                            break;
                        
                        default:
                            break;
                    }
                }
            }
            $store->save();
            DB::commit();
            $result['data'] = $store;
            return $result;
        } catch(\Exception $ex) {
            DB::rollBack();
            \Log::channel('daily')->error($ex->getMessage(), (array)$ex);
            return [
                'message' => trans('api.could_not_update'),
                'success' => false,
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR,
            ];
        }
    }

    /**
     * get store details
     * @param $id store ID
     * 
     * @return array $result
     */
    public function getDetails($id) {
        $result = [
            'message' => trans('api.get_data_success'),
            'success' => true,
            'statusCode' => Response::HTTP_OK,
            'data' => null,
        ];
        $store = Store::where('id', $id)->first();
        if (!$store) {
            $result['message'] = trans('api.not_found');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $result['data'] = $store;
        return $result;
    }

    /**
     * delete store
     * @param $id store ID
     * 
     * @return array $result
     */
    public function delete($id) {
        $store = Store::where('id', $id)->first();
        if (!$store) {
            $result['message'] = trans('api.not_found');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $user = auth('api')->user();
        if (!$user || ($user && $user->id != $store->user_id)) {
            return [
                'message' => trans('api.access_denied'),
                'success' => false,
                'statusCode' => Response::HTTP_FORBIDDEN,
            ];
        }
        $store->delete();
        $result = [
            'message' => trans('api.delete_succeeded'),
            'success' => true,
            'statusCode' => Response::HTTP_OK,
        ];
        return $result;
    }

    /**
     * get store list
     * @param User $user
     * 
     * @return Collection $paginate
     */
    public function getStoreList($user) {
        $dataQuery = request()->all();
        $perPage = isset($dataQuery['per_page']) ? $dataQuery['per_page'] : CommomHelper::PAGINATION_LIMIT;
        
        $builder = Store::where(function ($query) use ($dataQuery, $user) {
            if ($user && isset($dataQuery['myStores'])) {
                if ($dataQuery['myStores'] === 'true') {
                    $query->where('user_id', $user->id);
                } else {
                    $query->whereNot('user_id', $user->id);
                }
            }
            if (isset($dataQuery['name'])) {
                $query->where('name', 'LIKE', '%' . $dataQuery['name'] . '%');
            }
        });
        $paginate = $builder->paginate($perPage)->withQueryString(); 
        return $paginate;
    }
}