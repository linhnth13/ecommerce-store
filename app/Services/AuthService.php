<?php

namespace App\Services;

class AuthService
{
    /**
       * Get the correct authorization credentials field to be able to login with username or phone number.
       *
       * @param  string  $username
       * @param  string  $password
       * @return array
       */
    public static function credentials($username, $password)
    {
        if(is_numeric($username)){
            $field = 'phone_number';
            $username = self::convertPhoneNumber($username);
        } elseif (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        }else {
            $field = 'username';
        }
        $credentials = [$field => $username, 'password' => $password];
        return $credentials;
    }

    /**
     * Convert phone numbers to a template format (E.164) for easy comparison and saving
     *
     * @param  $phoneNumber
     * @return $phoneNumber removed + from format E.164 phone number
     */
    public static function convertPhoneNumber($phoneNumber) {
        if ($phoneNumber[0] !== '+') {
            $phoneNumber = '+' . $phoneNumber;
        }
        $phoneNumber = str_replace('+840', '84', $phoneNumber);
        $phoneNumber = str_replace('+', '', $phoneNumber);
        return $phoneNumber;
    }
}