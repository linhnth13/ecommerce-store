<?php

namespace App\Services;

use App\Models\Store;
use App\Models\User;
use App\Models\Product;
use App\CommomHelper;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProductService
{
    protected $defaultPath;
    
    public function __construct() {
        $this->defaultPath = "/media/products/";
    }

    /**
     * Create a new product
     * @param Request $request
     * @param User $user
     * 
     * @return array $result
     */
    public function create($request, $user) {
        $data = $request->all();
        $user = auth('api')->user();
        $store = Store::find($data['store_id']);
        if (!$store) {
            $result['message'] = trans('store.not_found_store');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $user = auth('api')->user();
        if (!$user || ($user && $user->id != $store->user_id)) {
            return [
                'message' => trans('api.access_denied'),
                'success' => false,
                'statusCode' => Response::HTTP_FORBIDDEN,
            ];
        }
        DB::beginTransaction();
        try{
            
            $dataInsert = [
                'store_id' => $data['store_id'],
                'name' => $data['name'],
                'description' => $data['description'],
                'status' => (int)$data['status'],
                'price' => $data['price']
            ];
            $newProduct = Product::create($dataInsert);

            DB::commit();
            return [
                'message' => trans('api.create_succeeded'),
                'success' => true,
                'data' => $newProduct
            ];
        } catch(\Exception $ex) {
            DB::rollBack();
            \Log::channel('daily')->error($ex->getMessage(), (array)$ex);
            return [
                'message' => trans('api.could_not_create'),
                'success' => false,
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR,
            ];
        }
    }

    /**
     * Update Product
     * @param Request $request
     * @param User $user
     * 
     * @return array $result
     */
    public function update($id, $request, $user) {
        $product = Product::where('id', $id)->with('store')->first();
        if (!$product) {
            $result['message'] = trans('api.not_found');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $user = auth('api')->user();
        if (!$user || ($user && $product->store && $user->id != $product->store->user_id)) {
            return [
                'message' => trans('api.access_denied'),
                'success' => false,
                'statusCode' => Response::HTTP_FORBIDDEN,
            ];
        }
        $result = [
            'message' => trans('api.update_succeeded'),
            'success' => true,
            'statusCode' => Response::HTTP_OK,
            'data' => null,
        ];
        DB::beginTransaction();
        try{
            $data = $request->all();
            foreach ($data as $field => $value) {
                if (!empty($value)) {
                    switch ($field) {
                        case 'name':
                            $product->name = $value;
                            break;
                        case 'description':
                            $product->description = $value;
                            break;
                        case 'status':
                            $product->status = (int)$value;
                            break;
                        case 'price':
                            $product->price = $value;
                            break;
                        default:
                            break;
                    }
                }
            }
            $product->save();
            DB::commit();
            $result['data'] = $product;
            return $result;
        } catch(\Exception $ex) {
            DB::rollBack();
            \Log::channel('daily')->error($ex->getMessage(), (array)$ex);
            return [
                'message' => trans('api.could_not_update'),
                'success' => false,
                'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR,
            ];
        }
    }

    /**
     * get product details
     * @param $id product ID
     * 
     * @return array $result
     */
    public function getDetails($id) {
        $result = [
            'message' => trans('api.get_data_success'),
            'success' => true,
            'statusCode' => Response::HTTP_OK,
            'data' => null,
        ];
        $product = Product::where('id', $id)->first();
        if (!$product) {
            $result['message'] = trans('api.not_found');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $result['data'] = $product;
        return $result;
    }

    /**
     * delete product
     * @param $id product ID
     * 
     * @return array $result
     */
    public function delete($id) {
        $user = auth('api')->user();
        $product = Product::where('id', $id)->with('store')->first();
        if (!$product) {
            $result['message'] = trans('api.not_found');
            $result['success'] = false;
            $result['statusCode'] = Response::HTTP_NOT_FOUND;
            return $result;
        }
        $user = auth('api')->user();
        if (!$user || ($user && $product->store && $user->id != $product->store->user_id)) {
            return [
                'message' => trans('api.access_denied'),
                'success' => false,
                'statusCode' => Response::HTTP_FORBIDDEN,
            ];
        }
        $product->delete();
        $result = [
            'message' => trans('api.delete_succeeded'),
            'success' => true,
            'statusCode' => Response::HTTP_OK,
        ];
        return $result;
    }

    /**
     * get product list
     * @param User $user
     * 
     * @return Collection $paginate
     */
    public function getProductList($user) {
        $dataQuery = request()->all();
        $perPage = isset($dataQuery['per_page']) ? $dataQuery['per_page'] : CommomHelper::PAGINATION_LIMIT;
        $storeIds = [];
        if ($user && isset($dataQuery['myStores'])) {
            if ($dataQuery['myStores'] === 'true') {
                $storeIds = Store::where('user_id', $user->id)->get()->pluck('id')->toArray();
            } else {
                $storeIds = Store::whereNot('user_id', $user->id)->get()->pluck('id')->toArray();
            }
        }
        
        $builder = Product::where(function ($query) use ($dataQuery, $user, $storeIds) {
            if (count($storeIds)) {
                $query->whereIn('store_id', $storeIds);
            }
            if (isset($dataQuery['name'])) {
                $query->where('name', 'LIKE', '%' . $dataQuery['name'] . '%');
            }
            if (isset($dataQuery['price_from'])) {
                $query->where('price', '>=', $dataQuery['price_from']);
            }
            if (isset($dataQuery['price_to'])) {
                $query->where('price', '<=', $dataQuery['price_to']);
            }
        });
        $builder->with('store');
        $paginate = $builder->paginate($perPage)->withQueryString(); 
        return $paginate;
    }
}