<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];
    public $table = 'stores';

    /**
     * Store belongs to users.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Store has many products.
     *
     * @return hasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'store_id', 'id');
    }

    /**
     * Get all of the store's media files.
     */
    public function mediaFiles()
    {
        return $this->morphMany(MediaFile::class, 'related');
    }
}
