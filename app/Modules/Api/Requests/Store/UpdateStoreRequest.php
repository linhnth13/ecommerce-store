<?php

namespace App\Modules\Api\Requests\Store;

use App\Modules\Api\Requests\Request;

class UpdateStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:100|unique:stores,name,{{id}}',
            'description' => 'nullable|string',
            'status' => 'nullable|boolean',
        ];
    }
}
