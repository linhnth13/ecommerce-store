<?php

namespace App\Modules\Api\Controllers;

use \Illuminate\Http\Request;
use App\Http\Resources\Product as ProductResource;
use App\Models\Product;
use App\Services\ProductService;
use App\Modules\Api\Requests\Product\CreateProductRequest;
use App\Modules\Api\Requests\Product\UpdateProductRequest;

class ProductController extends ApiController
{
    protected $productService;
    
    public function __construct(ProductService $productService) {
        $this->productService = $productService;
    }

    /**
    * @OA\Get(
    *      path="/api/v1/product/list",
    *      operationId="ProductList",
    *      tags={"Products"},
    *      summary="List Product with query strings",
    *      description="Product List",
    *      @OA\Parameter(
    *       name="Accept-Language",
    *       in="header",
    *       description="Set language parameter by RFC2616",
    *       @OA\Schema(
    *           type="string"
    *       ),
    *       example="vi"
    *      ),
    *      security={{"bearerAuth":{}}},
    *      @OA\Parameter(
    *         name="myStores",
    *         in="query",
    *         required=false,
    *         description="filter the stores owner of logged-in user",
    *         @OA\Schema(
    *             type="boolean",
    *             example="false"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="name",
    *         in="query",
    *         required=false,
    *         description="filter Product's name",
    *         @OA\Schema(
    *             type="string",
    *             example="Becker"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="price_from",
    *         in="query",
    *         required=false,
    *         description="filter price from",
    *         @OA\Schema(
    *             type="number",
    *             example="1000"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="price_to",
    *         in="query",
    *         required=false,
    *         description="filter price to",
    *         @OA\Schema(
    *             type="number",
    *             example="1000000"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="per_page",
    *         in="query",
    *         required=false,
    *         description="number rows of per page",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="page",
    *         in="query",
    *         required=false,
    *         description="page number",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    * @OA\Response(
    *     response="200",
    *     description="Successful response",
    *     @OA\JsonContent(
    *         type="object",
    *         @OA\Property(property="success", type="boolean", example=true),
    *         @OA\Property(property="message", type="string", example="get data success"),
    *         @OA\Property(property="data", type="object",
    *             @OA\Property(property="data", type="array",
    *                 @OA\Items(
    *                     @OA\Property(property="id", type="integer", example=91),
    *                     @OA\Property(property="name", type="string", example="Becker, Lubowitz and Beier"),
    *                     @OA\Property(property="description", type="string", example="Ea quia inventore suscipit voluptatum ut. Aut voluptas quidem blanditiis molestias quae est qui porro. Error veniam voluptas et a."),
    *                     @OA\Property(property="status", type="integer", example=1),
    *                     @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-13T23:03:15.000000Z"),
    *                     @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-13T23:03:15.000000Z")
    *                 )
    *             ),
    *             @OA\Property(property="links", type="object",
    *                 @OA\Property(property="first", type="string", example="http://localhost:8000/api/v1/product/list?myStores=true&name=Becker&page=1"),
    *                 @OA\Property(property="last", type="string", example="http://localhost:8000/api/v1/product/list?myStores=true&name=Becker&page=1"),
    *                 @OA\Property(property="prev", type="string", nullable=true),
    *                 @OA\Property(property="next", type="string", nullable=true)
    *             ),
    *             @OA\Property(property="meta", type="object",
    *                 @OA\Property(property="current_page", type="integer", example=1),
    *                 @OA\Property(property="from", type="integer", example=1),
    *                 @OA\Property(property="last_page", type="integer", example=1),
    *                 @OA\Property(property="links", type="array",
    *                     @OA\Items(
    *                         @OA\Property(property="url", type="string", nullable=true),
    *                         @OA\Property(property="label", type="string", example="&laquo; Previous"),
    *                         @OA\Property(property="active", type="boolean", example=false)
    *                     ),
    *                     @OA\Items(
    *                         @OA\Property(property="url", type="string", example="http://localhost:8000/api/v1/product/list?myStores=true&name=Becker&page=1"),
    *                         @OA\Property(property="label", type="string", example="1"),
    *                         @OA\Property(property="active", type="boolean", example=true)
    *                     ),
    *                     @OA\Items(
    *                         @OA\Property(property="url", type="string", nullable=true),
    *                         @OA\Property(property="label", type="string", example="Next &raquo;"),
    *                         @OA\Property(property="active", type="boolean", example=false)
    *                     )
    *                 ),
    *                 @OA\Property(property="path", type="string", example="http://localhost:8000/api/v1/product/list"),
    *                 @OA\Property(property="per_page", type="integer", example=20),
    *                 @OA\Property(property="to", type="integer", example=1),
    *                 @OA\Property(property="total", type="integer", example=1)
    *             )
    *         )
    *     )
    * ),
    *)
    */
    public function getProducts(Request $request) {
        $user = auth('api')->user();
        
        $products = $this->productService->getProductList($user, $request);
        return $this->respSuccess(ProductResource::collection($products)->response()->getData(true));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/product/create",
    *     operationId="createProduct",
    *     tags={"Products"},
    *     summary="Create new Product",
    *     description="Create new Product",
    *     security={{"bearerAuth":{}}},
    *     @OA\Parameter(
    *      name="Accept-Language",
    *      in="header",
    *      description="Set language parameter by RFC2616",
    *      @OA\Schema(
    *          type="string"
    *      ),
    *      example="vi"
    *     ),
    *     @OA\RequestBody(
    *         @OA\JsonContent(
    *               type="object",
    *               required={"store_id", "name", "description", "status", "price"},
    *               @OA\Property(property="store_id", type="number", description="ID of store", example="1"),
    *               @OA\Property(property="name", type="text", description="Name of Product - unique", example="Heniken"),
    *               @OA\Property(property="description", type="text", description="introduce Product", example="Heniken beer"),
    *               @OA\Property(property="status", type="boolean", description="status of Product: active/inactive", example="true"),
    *               @OA\Property(property="price", type="number", description="price of Product", example="1000000"),
    *         ),
    *    ),
    *       @OA\Response(
    *           response="200",
    *           description="Success response",
    *           @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=true),
    *               @OA\Property(property="message", type="string", example="Create succeeded!"),
    *               @OA\Property(
    *                   property="data",
    *                   type="object",
    *                   @OA\Property(property="id", type="integer", example=1001),
    *                   @OA\Property(property="name", type="string", example="Heniken"),
    *                   @OA\Property(property="description", type="string", example="Heniken beer"),
    *                   @OA\Property(property="status", type="integer", example=1),
    *                   @OA\Property(property="price", type="integer", example=1000000),
    *                   @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-15T00:39:19.000000Z"),
    *                   @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-15T00:39:19.000000Z"),
    *                   @OA\Property(
    *                       property="store",
    *                       type="object",
    *                       @OA\Property(property="id", type="integer", example=33),
    *                       @OA\Property(property="name", type="string", example="Morissette-Wyman"),
    *                       @OA\Property(property="description", type="string", example="Voluptatem in rerum occaecati at aut qui quis. Pariatur consequatur ducimus provident tempore est fugiat vero."),
    *                       @OA\Property(property="status", type="integer", example=1),
    *                       @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z"),
    *                       @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z")
    *                   )
    *               )
    *           )
    *       ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Validation error",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="The name field is required.."),
    *               @OA\Property(property="error_code", type="number", example=422),
    *          ),
    *       ),
    * )
    */
    public function createProduct(CreateProductRequest $request)
    {
        $user = auth('api')->user();
        $createProductResult = $this->productService->create($request, $user);
        if (!$createProductResult['success']){
            return $this->respError($createProductResult['message']);
        }
        $newProduct = $createProductResult['data'];
        $productResource = new ProductResource($newProduct);
        return $this->respSuccess($productResource, $createProductResult["message"]);
    }

    /**
     * @OA\Put(
     *     path="/api/v1/product/update/{id}",
     *     summary="Update a product",
     *     description="Update a product by ID",
     *     security={{"bearerAuth":{}}},
     *     tags={"Products"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the product to update",
     *         @OA\Schema(type="integer", format="int64", example=1)
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="data to be updated",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", description="Name of Product - unique", example="Heniken"),
     *             @OA\Property(property="description", type="string", description="Introduce Product", example="Heniken beer"),
     *             @OA\Property(property="status", type="boolean", description="Status of Product: active/inactive", example=true),
     *             @OA\Property(property="price", type="number", description="price of Product", example="1000000"),
     *         )
     *     ),
     *       @OA\Response(
    *           response="200",
    *           description="Success response",
    *           @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=true),
    *               @OA\Property(property="message", type="string", example="Update succeeded!"),
    *               @OA\Property(
    *                   property="data",
    *                   type="object",
    *                   @OA\Property(property="id", type="integer", example=1001),
    *                   @OA\Property(property="name", type="string", example="Heniken"),
    *                   @OA\Property(property="description", type="string", example="Heniken beer"),
    *                   @OA\Property(property="status", type="integer", example=1),
    *                   @OA\Property(property="price", type="integer", example=1000000),
    *                   @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-15T00:39:19.000000Z"),
    *                   @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-15T00:39:19.000000Z"),
    *                   @OA\Property(
    *                       property="store",
    *                       type="object",
    *                       @OA\Property(property="id", type="integer", example=33),
    *                       @OA\Property(property="name", type="string", example="Morissette-Wyman"),
    *                       @OA\Property(property="description", type="string", example="Voluptatem in rerum occaecati at aut qui quis. Pariatur consequatur ducimus provident tempore est fugiat vero."),
    *                       @OA\Property(property="status", type="integer", example=1),
    *                       @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z"),
    *                       @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z")
    *                   )
    *               )
    *           )
    *       ),
    *      @OA\Response(
    *          response=400,
    *          description="Access denied",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Access denied"),
    *               @OA\Property(property="error_code", type="number", example=400),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *          ),
    *       ),
    *      @OA\Response(
    *          response="404",
    *          description="Not found",
    *          @OA\JsonContent(
    *              type="object",
    *              @OA\Property(property="success", type="boolean", example=false),
    *              @OA\Property(property="message", type="string", example="Not found"),
    *              @OA\Property(property="error_code", type="integer", example=404)
    *          )
    *      ),
    *      @OA\Response(
    *          response=422,
    *          description="Validation error",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="The name field is required.."),
    *               @OA\Property(property="error_code", type="number", example=422),
    *          ),
    *       ),
    * )
    */
    public function updateProduct($id, UpdateProductRequest $request) {
        $user = auth('api')->user();
        $result = $this->productService->update($id, $request, $user);
        if (!$result['success']){
            return $this->respError($result['message']);
        }
        $product = $result['data'];
        $productResource = new ProductResource($product);
        return $this->respSuccess($productResource, $result["message"]);
    }

    /**
    * @OA\Get(
    *      path="/api/v1/product/details/{id}",
    *      operationId="ProductDetails",
    *      tags={"Products"},
    *      summary="Show product details",
    *      description="Product Details",
    *      @OA\Parameter(
    *       name="Accept-Language",
    *       in="header",
    *       description="Set language parameter by RFC2616",
    *       @OA\Schema(
    *           type="string"
    *       ),
    *       example="vi"
    *      ),
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         required=true,
    *         description="ID of the product",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Successful response",
    *         @OA\JsonContent(
    *             type="object",
    *             @OA\Property(property="success", type="boolean", example=true),
    *             @OA\Property(property="message", type="string", example="get data success"),
    *             @OA\Property(
    *                 property="data",
    *                 type="object",
    *                 @OA\Property(property="id", type="integer", example=1),
    *                 @OA\Property(property="name", type="string", example="assumenda"),
    *                 @OA\Property(property="description", type="string", example="Perspiciatis vel qui eveniet libero. Cum sed alias illum quo architecto. Velit molestias non ex assumenda est expedita. Quia quibusdam inventore vitae id omnis ut."),
    *                 @OA\Property(property="status", type="integer", example=0),
    *                 @OA\Property(property="price", type="integer", example=514000),
    *                 @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z"),
    *                 @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z"),
    *                 @OA\Property(
    *                     property="store",
    *                     type="object",
    *                     @OA\Property(property="id", type="integer", example=1),
    *                     @OA\Property(property="name", type="string", example="Botsford Ltd"),
    *                     @OA\Property(property="description", type="string", example="Aut quaerat animi sint quia fugiat. Expedita a nesciunt tempore velit quae velit. Magnam totam ad ut non velit qui possimus. Qui voluptatem ab voluptatem distinctio nulla veniam iusto."),
    *                     @OA\Property(property="status", type="integer", example=1),
    *                     @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z"),
    *                     @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-15T00:03:21.000000Z")
    *                 )
    *             )
    *         )
    *     ),
    *      @OA\Response(
    *          response=404,
    *          description="Not found",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Not found"),
    *               @OA\Property(property="error_code", type="number", example=404),
    *          ),
    *       ),
    *   )
    */
    public function showDetails($id) {
        $result = $this->productService->getDetails($id);
        if (!$result['success']){
            return $this->respError($result['message'], $result['statusCode']);
        }
        $productResource = new ProductResource($result['data']);
        return $this->respSuccess($productResource, $result['message']);
    }

    /**
    * @OA\Delete(
    *      path="/api/v1/product/delete/{id}",
    *      operationId="DeleteProduct",
    *      tags={"Products"},
    *      summary="Delete Product",
    *      description="Delete Product",
    *      security={{"bearerAuth":{}}},
    *      @OA\Parameter(
    *       name="Accept-Language",
    *       in="header",
    *       description="Set language parameter by RFC2616",
    *       @OA\Schema(
    *           type="string"
    *       ),
    *       example="vi"
    *      ),
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         required=true,
    *         description="ID of the product",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Successful response",
    *         @OA\JsonContent(
    *             type="object",
    *             @OA\Property(property="success", type="boolean", example=true),
    *             @OA\Property(property="message", type="string", example="Delete succeeded!"),
    *             @OA\Property(property="data", type="array", @OA\Items(type="string"))
    *         )
    *     ),
    *      @OA\Response(
    *          response=400,
    *          description="Access denied",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Access denied"),
    *               @OA\Property(property="error_code", type="number", example=400),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=404,
    *          description="Not found",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Not found"),
    *               @OA\Property(property="error_code", type="number", example=404),
    *          ),
    *       ),
    * )
    */
    public function delete($id) {
        $result = $this->productService->delete($id);
        if (!$result['success']){
            return $this->respError($result['message'], $result['statusCode']);
        }
        return $this->respSuccess([], $result['message']);
    }
}
