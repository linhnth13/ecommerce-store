<?php

namespace App\Modules\Api\Controllers;

use \Illuminate\Http\Request;
use App\Http\Resources\Store as StoreResource;
use App\Services\StoreService;
use App\Modules\Api\Requests\Store\CreateStoreRequest;
use App\Modules\Api\Requests\Store\UpdateStoreRequest;

class StoreController extends ApiController
{
    protected $storeService;
    
    public function __construct(StoreService $storeService) {
        $this->storeService = $storeService;
    }

    /**
    * @OA\Get(
    *      path="/api/v1/store/list",
    *      operationId="StoreList",
    *      tags={"Stores"},
    *      summary="List store with query strings",
    *      description="Store List",
    *      @OA\Parameter(
    *       name="Accept-Language",
    *       in="header",
    *       description="Set language parameter by RFC2616",
    *       @OA\Schema(
    *           type="string"
    *       ),
    *       example="vi"
    *      ),
    *      security={{"bearerAuth":{}}},
    *      @OA\Parameter(
    *         name="myStores",
    *         in="query",
    *         required=false,
    *         description="filter the stores owner of logged-in user",
    *         @OA\Schema(
    *             type="boolean",
    *             example="false"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="name",
    *         in="query",
    *         required=false,
    *         description="filter store's name",
    *         @OA\Schema(
    *             type="string",
    *             example="Becker"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="per_page",
    *         in="query",
    *         required=false,
    *         description="number rows of per page",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    *      @OA\Parameter(
    *         name="page",
    *         in="query",
    *         required=false,
    *         description="page number",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    * @OA\Response(
    *     response="200",
    *     description="Successful response",
    *     @OA\JsonContent(
    *         type="object",
    *         @OA\Property(property="success", type="boolean", example=true),
    *         @OA\Property(property="message", type="string", example="get data success"),
    *         @OA\Property(property="data", type="object",
    *             @OA\Property(property="data", type="array",
    *                 @OA\Items(
    *                     @OA\Property(property="id", type="integer", example=91),
    *                     @OA\Property(property="name", type="string", example="Becker, Lubowitz and Beier"),
    *                     @OA\Property(property="description", type="string", example="Ea quia inventore suscipit voluptatum ut. Aut voluptas quidem blanditiis molestias quae est qui porro. Error veniam voluptas et a."),
    *                     @OA\Property(property="status", type="integer", example=1),
    *                     @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-13T23:03:15.000000Z"),
    *                     @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-13T23:03:15.000000Z")
    *                 )
    *             ),
    *             @OA\Property(property="links", type="object",
    *                 @OA\Property(property="first", type="string", example="http://localhost:8000/api/v1/store/list?myStores=true&name=Becker&page=1"),
    *                 @OA\Property(property="last", type="string", example="http://localhost:8000/api/v1/store/list?myStores=true&name=Becker&page=1"),
    *                 @OA\Property(property="prev", type="string", nullable=true),
    *                 @OA\Property(property="next", type="string", nullable=true)
    *             ),
    *             @OA\Property(property="meta", type="object",
    *                 @OA\Property(property="current_page", type="integer", example=1),
    *                 @OA\Property(property="from", type="integer", example=1),
    *                 @OA\Property(property="last_page", type="integer", example=1),
    *                 @OA\Property(property="links", type="array",
    *                     @OA\Items(
    *                         @OA\Property(property="url", type="string", nullable=true),
    *                         @OA\Property(property="label", type="string", example="&laquo; Previous"),
    *                         @OA\Property(property="active", type="boolean", example=false)
    *                     ),
    *                     @OA\Items(
    *                         @OA\Property(property="url", type="string", example="http://localhost:8000/api/v1/store/list?myStores=true&name=Becker&page=1"),
    *                         @OA\Property(property="label", type="string", example="1"),
    *                         @OA\Property(property="active", type="boolean", example=true)
    *                     ),
    *                     @OA\Items(
    *                         @OA\Property(property="url", type="string", nullable=true),
    *                         @OA\Property(property="label", type="string", example="Next &raquo;"),
    *                         @OA\Property(property="active", type="boolean", example=false)
    *                     )
    *                 ),
    *                 @OA\Property(property="path", type="string", example="http://localhost:8000/api/v1/store/list"),
    *                 @OA\Property(property="per_page", type="integer", example=20),
    *                 @OA\Property(property="to", type="integer", example=1),
    *                 @OA\Property(property="total", type="integer", example=1)
    *             )
    *         )
    *     )
    * ),
    *)
    */
    public function getStores(Request $request) {
        $user = auth('api')->user();
        
        $stores = $this->storeService->getStoreList($user, $request);
        return $this->respSuccess(StoreResource::collection($stores)->response()->getData(true));
    }

    /**
    * @OA\Post(
    *     path="/api/v1/store/create",
    *     operationId="createStore",
    *     tags={"Stores"},
    *     summary="Create new Store",
    *     description="Create new Store",
    *     security={{"bearerAuth":{}}},
    *     @OA\Parameter(
    *      name="Accept-Language",
    *      in="header",
    *      description="Set language parameter by RFC2616",
    *      @OA\Schema(
    *          type="string"
    *      ),
    *      example="vi"
    *     ),
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"name", "description", "status"},
    *               @OA\Property(property="name", type="text", description="Name of Store - unique", example="Heniken Store"),
    *               @OA\Property(property="description", type="text", description="introduce store", example="Heniken Store is a beer store"),
    *               @OA\Property(property="status", type="boolean", description="status of store: active/inactive", example="1"),
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response="200",
    *          description="Successful response",
    *          @OA\JsonContent(
    *              type="object",
    *              @OA\Property(property="success", type="boolean", example=true),
    *              @OA\Property(property="message", type="string", example="Create succeeded!"),
    *              @OA\Property(property="data", type="object",
    *                  @OA\Property(property="id", type="integer", example=101),
    *                  @OA\Property(property="name", type="string", example="Heniken Store"),
    *                  @OA\Property(property="description", type="string", example="Heniken Store is a beer store"),
    *                  @OA\Property(property="status", type="integer", example=0),
    *                  @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-14T21:34:56.000000Z"),
    *                  @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-14T21:34:56.000000Z")
    *              )
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Validation error",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="The name field is required.."),
    *               @OA\Property(property="error_code", type="number", example=422),
    *          ),
    *       ),
    * )
    */
    public function createStore(CreateStoreRequest $request)
    {
        $user = auth('api')->user();
        $createStoreResult = $this->storeService->create($request, $user);
        if (!$createStoreResult['success']){
            return $this->respError($createStoreResult['message']);
        }
        $newStore = $createStoreResult['data'];
        $storeResource = new StoreResource($newStore);
        return $this->respSuccess($storeResource, $createStoreResult["message"]);
    }

    /**
     * @OA\Put(
     *     path="/api/v1/store/update/{id}",
     *     summary="Update a store",
     *     description="Update a store by ID",
     *     security={{"bearerAuth":{}}},
     *     tags={"Stores"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the store to update",
     *         @OA\Schema(type="integer", format="int64", example=1)
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Store data to be updated",
     *         @OA\JsonContent(
     *             @OA\Property(property="name", type="string", description="Name of Store - unique", example="Heniken Store"),
     *             @OA\Property(property="description", type="string", description="Introduce store", example="Heniken Store is a beer store"),
     *             @OA\Property(property="status", type="boolean", description="Status of store: active/inactive", example=true)
     *         )
     *     ),
    *      @OA\Response(
    *          response="200",
    *          description="Successful response",
    *          @OA\JsonContent(
    *              type="object",
    *              @OA\Property(property="success", type="boolean", example=true),
    *              @OA\Property(property="message", type="string", example="Update succeeded!"),
    *              @OA\Property(property="data", type="object",
    *                  @OA\Property(property="id", type="integer", example=101),
    *                  @OA\Property(property="name", type="string", example="Heniken Store"),
    *                  @OA\Property(property="description", type="string", example="Heniken Store is a beer store"),
    *                  @OA\Property(property="status", type="integer", example=1),
    *                  @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-14T21:34:56.000000Z"),
    *                  @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-14T21:34:56.000000Z")
    *              )
    *          )
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Access denied",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Access denied"),
    *               @OA\Property(property="error_code", type="number", example=400),
    *          ),
    *       ),
    *      @OA\Response(
    *          response="404",
    *          description="Not found",
    *          @OA\JsonContent(
    *              type="object",
    *              @OA\Property(property="success", type="boolean", example=false),
    *              @OA\Property(property="message", type="string", example="Not found"),
    *              @OA\Property(property="error_code", type="integer", example=404)
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=422,
    *          description="Validation error",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="The name field is required.."),
    *               @OA\Property(property="error_code", type="number", example=422),
    *          ),
    *       ),
    * )
    */
    public function updateStore($id, UpdateStoreRequest $request) {
        $user = auth('api')->user();
        $result = $this->storeService->update($id, $request, $user);
        if (!$result['success']){
            return $this->respError($result['message']);
        }
        $store = $result['data'];
        $storeResource = new StoreResource($store);
        return $this->respSuccess($storeResource, $result["message"]);
    }

    /**
    * @OA\Get(
    *      path="/api/v1/store/details/{id}",
    *      operationId="StoreDetails",
    *      tags={"Stores"},
    *      summary="Show store details",
    *      description="Store Details",
    *      @OA\Parameter(
    *       name="Accept-Language",
    *       in="header",
    *       description="Set language parameter by RFC2616",
    *       @OA\Schema(
    *           type="string"
    *       ),
    *       example="vi"
    *      ),
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         required=true,
    *         description="ID of the store",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Successful response",
    *         @OA\JsonContent(
    *             type="object",
    *             @OA\Property(property="success", type="boolean", example=true),
    *             @OA\Property(property="message", type="string", example="get data success"),
    *             @OA\Property(property="data", type="object",
    *                 @OA\Property(property="id", type="integer", example=1),
    *                 @OA\Property(property="name", type="string", example="Ziemann Group"),
    *                 @OA\Property(property="description", type="string", example="Laborum eligendi sunt corporis ut aut. Earum voluptatem illum voluptatum vitae optio."),
    *                 @OA\Property(property="status", type="integer", example=0),
    *                 @OA\Property(property="created_at", type="string", format="date-time", example="2023-08-13T23:03:15.000000Z"),
    *                 @OA\Property(property="updated_at", type="string", format="date-time", example="2023-08-13T23:03:15.000000Z")
    *             )
    *         )
    *     ),
    *      @OA\Response(
    *          response=404,
    *          description="Not found",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Not found"),
    *               @OA\Property(property="error_code", type="number", example=404),
    *          ),
    *       ),
    *   )
    */
    public function showDetails($id) {
        $result = $this->storeService->getDetails($id);
        if (!$result['success']){
            return $this->respError($result['message'], $result['statusCode']);
        }
        $storeResource = new StoreResource($result['data']);
        return $this->respSuccess($storeResource, $result['message']);
    }

    /**
    * @OA\Delete(
    *      path="/api/v1/store/delete/{id}",
    *      operationId="DeleteStore",
    *      tags={"Stores"},
    *      summary="Delete store",
    *      description="Delete store",
    *      security={{"bearerAuth":{}}},
    *      @OA\Parameter(
    *       name="Accept-Language",
    *       in="header",
    *       description="Set language parameter by RFC2616",
    *       @OA\Schema(
    *           type="string"
    *       ),
    *       example="vi"
    *      ),
    *      @OA\Parameter(
    *         name="id",
    *         in="path",
    *         required=true,
    *         description="ID of the store",
    *         @OA\Schema(
    *             type="integer"
    *         )
    *     ),
    *     @OA\Response(
    *         response="200",
    *         description="Successful response",
    *         @OA\JsonContent(
    *             type="object",
    *             @OA\Property(property="success", type="boolean", example=true),
    *             @OA\Property(property="message", type="string", example="Delete succeeded!"),
    *             @OA\Property(property="data", type="array", @OA\Items(type="string"))
    *         )
    *     ),
    *      @OA\Response(
    *          response=400,
    *          description="Access denied",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Access denied"),
    *               @OA\Property(property="error_code", type="number", example=400),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *          ),
    *       ),
    *      @OA\Response(
    *          response=404,
    *          description="Not found",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Not found"),
    *               @OA\Property(property="error_code", type="number", example=404),
    *          ),
    *       ),
    * )
    */
    public function delete($id) {
        $result = $this->storeService->delete($id);
        if (!$result['success']){
            return $this->respError($result['message'], $result['statusCode']);
        }
        return $this->respSuccess([], $result['message']);
    }
}
