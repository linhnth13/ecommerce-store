<?php

namespace App\Modules\Api\Controllers;

use App\Models\User;
use App\Modules\Api\Requests\Auth\LoginRequest;
use \Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\Services\AuthService;

class AuthController extends ApiController
{
    /**
    * @OA\Post(
    * path="/api/v1/auth/login",
    * operationId="authLogin",
    * tags={"Authentication"},
    * summary="User Login",
    * description="Login User Here",
    * @OA\Parameter(
    *  name="Accept-Language",
    *  in="header",
    *  description="Set language parameter by RFC2616",
    *  @OA\Schema(
    *      type="string"
    *  ),
    *  example="vi"
    * ),
    *     @OA\RequestBody(
    *         @OA\JsonContent(),
    *         @OA\MediaType(
    *            mediaType="multipart/form-data",
    *            @OA\Schema(
    *               type="object",
    *               required={"username", "password"},
    *               @OA\Property(property="username", type="text", description="username or phone number or email of user"),
    *               @OA\Property(property="password", type="password")
    *            ),
    *        ),
    *    ),
    *      @OA\Response(
    *          response=200,
    *          description="Login successfully",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=true),
    *               @OA\Property(property="message", type="string", example="Login successful"),
    *               @OA\Property(property="data", type="string", format="array", example={"user":{
    *                   "id": 9,
    *                   "username": "ellsworth.stracke",
    *                   "name": "Ms. Jayda Schmidt",
    *                   "phone_number": "15516549444",
    *                   "email": "abdullah70@example.org",
    *                   "created_at": "2023-08-13T00:00:50.000000Z",
    *                   "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..."
    *               }}),
    *           ),
    *       ),
    *      @OA\Response(
    *         response=400,
    *          description="Incorrect username or password",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=false),
    *               @OA\Property(property="message", type="string", example="Incorrect username or password"),
    *               @OA\Property(property="error_code", type="number", example=400),
    *          ),
    *       ),
    * )
    */
    public function login(LoginRequest $request)
    {
        $credentials = AuthService::credentials(...$request->only('username', 'password'));
        $token = auth('api')->attempt($credentials);

        if ($token) {
            /** @var user User */
            $user = auth('api')->user();
            $userResouces = new UserResource($user);
            $message = trans('auth.login_successful');
            return $this->respWithToken($userResouces, $token, $message);
        }

        return $this->respError(trans('auth.incorrect_username_password'));
    }

    /** @OA\Post(
    * path="/api/v1/auth/user/logout",
    * operationId="LogoutUser",
    * tags={"Authentication"},
    * summary="Logout User",
    * description="Logout User",
    * security={{"bearerAuth":{}}},
    * @OA\Parameter(
    *  name="Accept-Language",
    *  in="header",
    *  description="Set language parameter by RFC2616",
    *  @OA\Schema(
    *      type="string"
    *  ),
    *  example="vi"
    * ),
    *     @OA\RequestBody(
    *     ),
    *     @OA\Response(
    *          response=200,
    *          description="Logout successful",
    *          @OA\JsonContent(
    *               @OA\Property(property="success", type="boolean", example=true),
    *               @OA\Property(property="message", type="string", example="Logout successful"),
    *               @OA\Property(property="data", type="string", format="array", example={}),
    *          )
    *      ),
    *      @OA\Response(
    *          response=401,
    *          description="Unauthenticated.",
    *          @OA\JsonContent(
    *               @OA\Property(property="message", type="string", example="Unauthenticated."),
    *           )
    *      ),
    * )
    */
    public function logout(Request $request)
    {
        auth()->guard('api')->logout();
        return $this->respSuccess([], trans('auth.logout_successful'));
    }

    protected function respWithToken($user, $token, $message=null)
    {
        $user['token'] = $token;
        return $this->respSuccess(["user" => $user], $message);
    }
}
