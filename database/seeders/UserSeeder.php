<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Services\AuthService;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(Faker $faker): void
    {
        DB::transaction(function () use ($faker) {
            $data = [];
            $chunkSize = 1000;
            if (User::count() === 0) {
                for ($i = 1; $i <= $chunkSize; $i++) {
                    $data[] = [
                        'name' => $faker->name,
                        'username' => $faker->unique()->userName(),
                        'email' => $faker->unique()->safeEmail(),
                        'phone_number' => AuthService::convertPhoneNumber($faker->unique()->e164PhoneNumber()),
                        'avatar' => $faker->imageUrl(),
                        'password' => '$2y$10$ixdtHymh3uOEbKuWtCmns.EFJ4GuQspK6wuiqkTMSqoEZTnF3pagS', // 123123
                        'created_at' => now()->toDateTimeString(),
                        'updated_at' => now()->toDateTimeString(),
                    ];
                }
                User::insert($data);
            }
        });
    }
}
