<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\Models\User;
use App\Models\Store;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class StoreProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(Faker $faker): void
    {
        DB::transaction(function () use ($faker) {
            $numberUsers = 10;
            $chunkSize = 10;
            $users = User::doesnthave('stores')->limit($numberUsers)->get();
            $storeData = [];
            $productData = [];
            if (User::count() > 0) {
                foreach ($users as $user) {
                    for ($i = 1; $i <= $chunkSize; $i++) {
                        $storeData[] = [
                            'user_id' => $user->id,
                            'name' => $faker->unique()->company,
                            'description' => $faker->text,
                            'status' => rand(0, 1),
                            'created_at' => now()->toDateTimeString(),
                            'updated_at' => now()->toDateTimeString(),
                        ];
                    }
                    Store::insert($storeData);
                    unset($storeData);
                    $storeData = [];
                    $totalProductPerStore = 10;
                    $stores = Store::where('user_id', $user->id)->get();
                    foreach ($stores as $store) {
                        for ($i = 1; $i <= $totalProductPerStore; $i++) {
                            $productData[] = [
                                'store_id' => $store->id,
                                'name' => $faker->word,
                                'description' => $faker->text,
                                'price' => rand(10, 1000) * 1000, //thousands VND
                                'status' => rand(0, 1),
                                'created_at' => now()->toDateTimeString(),
                                'updated_at' => now()->toDateTimeString(),
                            ];
                        }
                        Product::insert($productData);
                        unset($productData);
                        $productData = [];
                    }
                    unset($stores);
                }
            }
        });
    }
}
