<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->after('name')->unique();
            $table->string('phone_number')->after('email_verified_at')->unique()->nullable();
            $table->timestamp('phone_verified_at')->after('phone_number')->nullable();
            $table->string('avatar')->after('phone_verified_at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('username');
            $table->dropColumn('phone_number');
            $table->dropColumn('phone_verified_at');
            $table->dropColumn('avatar');
            $table->dropColumn('deleted_at');
        });
    }
};
